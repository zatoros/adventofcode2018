#include<iostream>
#include<fstream>
#include<string>
#include<algorithm>
#include<utility>
#include<map>
#include<stack>

using namespace std;

enum FieldType
{
    OPEN = '.',
    TREES = '|',
    LUMBERYARD = '#'
};

typedef map<int, map<int,FieldType> > ForestMap;

static const int TIME_SPAN = 10000;
 
typedef FieldType (*FieldHandler)(ForestMap& forestMap, int x, int y);

FieldType handleOpen(ForestMap& forestMap, int x, int y)
{
    int xSize = forestMap[0].size();
    int ySize = forestMap.size();

    int trees = 0;
    
    if(x - 1 >= 0 && forestMap[y][x-1] == TREES)
        trees++;
    if(x - 1 >= 0 && y - 1 >= 0 && forestMap[y-1][x-1] == TREES )
        trees++;
    if(x - 1 >= 0 && y + 1 < ySize && forestMap[y+1][x-1] == TREES )
        trees++;        
    if(x + 1 < xSize && forestMap[y][x+1] == TREES)
        trees++;    
    if(x + 1 < xSize && y - 1 >= 0 && forestMap[y-1][x+1] == TREES )
        trees++;
    if(x + 1 < xSize && y + 1 < ySize && forestMap[y+1][x+1] == TREES )
        trees++;              
    if(y - 1 >= 0 && forestMap[y-1][x] == TREES)
        trees++; 
    if(y + 1 < ySize && forestMap[y+1][x] == TREES)
        trees++;         

    if(trees >= 3)
        return TREES;
    return OPEN;
}

FieldType handleTrees(ForestMap& forestMap, int x, int y)
{
    int xSize = forestMap[0].size();
    int ySize = forestMap.size();

    int lumberYards = 0;

    if(x - 1 >= 0 && forestMap[y][x-1] == LUMBERYARD)
        lumberYards++;
    if(x - 1 >= 0 && y - 1 >= 0 && forestMap[y-1][x-1] == LUMBERYARD )
        lumberYards++;
    if(x - 1 >= 0 && y + 1 < ySize && forestMap[y+1][x-1] == LUMBERYARD )
        lumberYards++;        
    if(x + 1 < xSize && forestMap[y][x+1] == LUMBERYARD)
        lumberYards++;    
    if(x + 1 < xSize && y - 1 >= 0 && forestMap[y-1][x+1] == LUMBERYARD )
        lumberYards++;
    if(x + 1 < xSize && y + 1 < ySize && forestMap[y+1][x+1] == LUMBERYARD )
        lumberYards++;              
    if(y - 1 >= 0 && forestMap[y-1][x] == LUMBERYARD)
        lumberYards++; 
    if(y + 1 < ySize && forestMap[y+1][x] == LUMBERYARD)
        lumberYards++;         
        
    if(lumberYards >= 3)
        return LUMBERYARD;
    return TREES;
}

FieldType handleLumberYard(ForestMap& forestMap, int x, int y)
{
    int xSize = forestMap[0].size();
    int ySize = forestMap.size();

    bool haveTrees = false, haveLumberYard = false;

    if(x - 1 >= 0)
    {
        if(forestMap[y][x-1] == LUMBERYARD)
            haveLumberYard = true;
        else if(forestMap[y][x-1] == TREES)
            haveTrees = true;
    }
    if(x - 1 >= 0 && y - 1 >= 0)
    {
        if(forestMap[y-1][x-1] == LUMBERYARD)
            haveLumberYard = true;
        else if(forestMap[y-1][x-1] == TREES)
            haveTrees = true;
    }
    if(x - 1 >= 0 && y + 1 < ySize)
    {
        if(forestMap[y+1][x-1] == LUMBERYARD)
            haveLumberYard = true;
        else if(forestMap[y+1][x-1] == TREES)
            haveTrees = true;
    }
    if(x + 1 < xSize)
    {
        if(forestMap[y][x+1] == LUMBERYARD)
            haveLumberYard = true;
        else if(forestMap[y][x+1] == TREES)
            haveTrees = true;
    }      
    if(x + 1 < xSize && y - 1 >= 0)
    {
        if(forestMap[y-1][x+1] == LUMBERYARD)
            haveLumberYard = true;
        else if(forestMap[y-1][x+1] == TREES)
            haveTrees = true;
    }
    if(x + 1 < xSize && y + 1 < ySize)
    {
        if(forestMap[y+1][x+1] == LUMBERYARD)
            haveLumberYard = true;
        else if(forestMap[y+1][x+1] == TREES)
            haveTrees = true;
    }    
    if(y - 1 >= 0)
    {
        if(forestMap[y-1][x] == LUMBERYARD)
            haveLumberYard = true;
        else if(forestMap[y-1][x] == TREES)
            haveTrees = true;
    }
    if(y + 1 < ySize)
    {
        if(forestMap[y+1][x] == LUMBERYARD)
            haveLumberYard = true;
        else if(forestMap[y+1][x] == TREES)
            haveTrees = true;
    }

    if(haveTrees && haveLumberYard)
        return LUMBERYARD;
    return OPEN;
}

void printForestMap(ForestMap& forestMap)
{
    for(auto itY = forestMap.begin(); itY != forestMap.end(); itY++)
    {
        for(auto itX = itY->second.begin(); itX != itY->second.end(); itX++)
        {
            cout << static_cast<char>(itX->second);
        }
        cout << endl;
    }
}

void printResult(ForestMap& forestMap)
{
    int trees = 0, lumberYeards = 0;
    for(auto itY = forestMap.begin(); itY != forestMap.end(); itY++)
    {
        for(auto itX = itY->second.begin(); itX != itY->second.end(); itX++)
        {
            if(itX->second == TREES)
                trees++;
            else if(itX->second == LUMBERYARD)
                lumberYeards++;
        }
    }

    cout << trees << "  " << lumberYeards << "    " << trees*lumberYeards << endl;
}

int main(void)
{
    ForestMap forestMap, forestMapNew;

    string input;
    int x;
    int y = 0;
    while( getline(cin, input) )
    {
        x = 0;
        for(auto it = input.begin(); it != input.end(); it++)
        {
            if(!isspace(*it))
                forestMap[y][x++] = static_cast<FieldType>(*it);
        }
        y++;
    }

    map< FieldType, FieldHandler > fieldHandlers;
    fieldHandlers.insert( make_pair(OPEN,&handleOpen) );
    fieldHandlers.insert( make_pair(TREES,&handleTrees) );
    fieldHandlers.insert( make_pair(LUMBERYARD,&handleLumberYard) );

    ForestMap * currentMap, * futureMap;
    currentMap = &forestMap;
    futureMap = &forestMapNew;

    const int Y_SIZE = currentMap->size();
    const int X_SIZE = currentMap->at(0).size();

    for(int i = 0; i < TIME_SPAN; i++)
    {
        for(int y = 0; y < Y_SIZE; y++)
        {
            for(int x = 0; x < X_SIZE; x++)
            {
                (*futureMap)[y][x] = fieldHandlers[(*currentMap)[y][x]](*currentMap, x, y);
            }
        }
        ForestMap * temp = currentMap;
        currentMap = futureMap;
        futureMap = temp;
        cout << i << "    ";
        printResult(*currentMap);
    }

    printForestMap(*currentMap);

    //Rest resolved "on paper" - pattern detected in output
    return 0;
}