#include<iostream>
#include<fstream>
#include<string>
#include<algorithm>
#include<utility>
#include<map>
#include<stack>

using namespace std;

typedef pair<int, int> Point;

enum FieldType
{
    SAND = 0,
    CLAY,
    WATER_REST,
    WATER
};

typedef map<int, map<int, FieldType> > WaterMap;

void printWaterMap(WaterMap& waterMap, int xMin = 490, int xMax = 510, int yMin = 0, int yMax = 20)
{
    static int i = 0;
    string filename( to_string(i) );
    filename += ".txt";

    ofstream out(filename);

    for(int y = yMin; y < yMax; y++)
    {
        for(int x = xMin; x < xMax; x++)
        {
            switch(waterMap[y][x])
            {
                case CLAY:
                    out << "#";
                    break;
                case WATER:
                    out << "|";
                    break;
                case WATER_REST:
                    out << "~";
                    break;
                case SAND:
                    out << ".";
                    break;

            }
        }
        out << endl;
    }
    i++;
}

void loadRange(string::iterator it1, string::iterator it2, Point& range, string& input)
{
    range.first = stoi(std::string(it1+2,it2));
    it1 = it2+2;
    while( it1 != input.end() && isdigit(*(it1++)));
    range.second = stoi(std::string(it2+2, it1));
}

void goDown(WaterMap& waterMap, Point currPoint, Point yRange);
bool goLeft(WaterMap& waterMap, Point currPoint, Point yRange);
bool goRight(WaterMap& waterMap, Point currPoint, Point yRange);

bool checkEnd(Point point, Point yRange)
{
    if(point.first > yRange.second)
    {
        return true;
    }
    return false;
}

Point restWater(WaterMap& waterMap, Point point)
{
    Point sourcePoint;
    int leftRange, rightRange;
    leftRange = rightRange = point.second;
    while(waterMap[point.first][--leftRange] == WATER);
    while(waterMap[point.first][++rightRange] == WATER);

    for(int i = leftRange + 1; i < rightRange; i++)
    {
        Point upperPoint(point.first - 1, i);
        waterMap[point.first][i] = WATER_REST;
        if(waterMap[upperPoint.first][upperPoint.second] == WATER)
            sourcePoint = upperPoint;
    }
    return sourcePoint;
}

void goDown(WaterMap& waterMap, Point currPoint, Point yRange)
{
    Point nextPoint(currPoint.first + 1, currPoint.second);
    while( waterMap[nextPoint.first][nextPoint.second] != CLAY &&
           waterMap[nextPoint.first][nextPoint.second] != WATER_REST &&
           !checkEnd(nextPoint, yRange) )
    {
        currPoint = nextPoint;
        waterMap[currPoint.first][currPoint.second] = WATER;
        nextPoint.first++;
    }
    
    if(checkEnd(nextPoint, yRange))
    {
        return;
    }
    else
    {
        bool waterRest = goLeft(waterMap, currPoint, yRange);
        waterRest = goRight(waterMap, currPoint, yRange) && waterRest;
        if( waterRest )
        {
            currPoint = restWater(waterMap, currPoint);
            goDown(waterMap, currPoint, yRange);
        }
    }
}

bool goLeft(WaterMap& waterMap, Point currPoint, Point yRange)
{
    Point nextPoint(currPoint.first, currPoint.second - 1);
    while(waterMap[nextPoint.first][nextPoint.second] != CLAY && 
          waterMap[nextPoint.first][nextPoint.second] != WATER_REST)
    {
        currPoint = nextPoint;
        waterMap[currPoint.first][currPoint.second] = WATER;
        Point checkDownPoint(currPoint.first + 1, currPoint.second);
        if(waterMap[checkDownPoint.first][checkDownPoint.second] != CLAY && 
           waterMap[checkDownPoint.first][checkDownPoint.second] != WATER_REST)
        {
            goDown(waterMap, currPoint, yRange);
            return false;
        }        
        nextPoint.second--;
    }
    return true;
}

bool goRight(WaterMap& waterMap, Point currPoint, Point yRange)
{
    Point nextPoint(currPoint.first, currPoint.second + 1);
    while(waterMap[nextPoint.first][nextPoint.second] != CLAY && 
          waterMap[nextPoint.first][nextPoint.second] != WATER_REST)
    {
        currPoint = nextPoint;
        waterMap[currPoint.first][currPoint.second] = WATER;
        Point checkDownPoint(currPoint.first + 1, currPoint.second);
        if(waterMap[checkDownPoint.first][checkDownPoint.second] != CLAY && 
           waterMap[checkDownPoint.first][checkDownPoint.second] != WATER_REST)
        {
            goDown(waterMap, currPoint, yRange);
            return false;
        }        
        nextPoint.second++;
    }
    return true;
}

int main(void)
{
    WaterMap waterMap;

    string input;
    while( getline(cin, input) )
    {
        int point = -1;
        Point range(-1,-1);
        auto it1 = find(input.begin(), input.end(), 'x');
        auto it2 = find(input.begin(), input.end(), 'y');
        auto it3 = find(input.begin(), input.end(), '.');
        auto it4 = find(input.begin(), input.end(), ',');
        if(it1 > it2)
        {
            //x is range
            loadRange(it1, it3, range, input);
            point = stoi(std::string(it2+2,it4));

            for(int x = range.first; x <= range.second; x++)
            {
                waterMap[point][x] = CLAY;
            }
        }
        else
        {
            //y is range
            loadRange(it2, it3, range, input);
            point = stoi(std::string(it1+2,it4));
            for(int y = range.first; y <= range.second; y++)
            {
                waterMap[y][point] = CLAY;
            }
        }
    }

    Point yRange(waterMap.begin()->first, waterMap.rbegin()->first);
    Point waterSource(0, 500);
    int streams = 1;

    goDown(waterMap, waterSource, yRange);

    printWaterMap(waterMap, 0, 1000, 0, yRange.second + 10);

    long result1 = 0, result2 = 0;
    for(int y = yRange.first; y <= yRange.second; y++)
    {
        for(auto it = waterMap[y].begin(); it != waterMap[y].end(); it++)
        {
            if(it->second == WATER_REST || it->second == WATER)
                result1++;
            if(it->second == WATER_REST)
                result2++;
        }
    }

    cout << result1 << endl;
    cout << result2 << endl;
    return 0;
}