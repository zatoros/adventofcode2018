#include<iostream>
#include<stack>
#include<deque>
#include<list>
#include<algorithm>
#include<set>
#include<utility>

const int PATH_LENGTH = 1000;

typedef std::pair<int, int> Direction;

Direction operator+(const Direction& l, const Direction& r)
{
    return std::make_pair(l.first+r.first, l.second+r.second);
}

Direction& operator+=(Direction& l, const Direction&r)
{
    l.first += r.first;
    l.second += r.second;
    return l;
}

struct DirectionUtil
{
    static Direction isDirection(char letter)
    {
        switch(letter)
        {
            case 'N':
                return std::make_pair(1,0);
            case 'S':
                return std::make_pair(-1,0);
            case 'W':
                return std::make_pair(0,1);
            case 'E':
                return std::make_pair(0,-1);
            default:
                return std::make_pair(0,0);
        }
    }
};

struct Result
{
    int value;
    Direction location;
    
    Result() : value(0), location(0,0) {}
    
    Result operator+(const Result& other) const
    {
        Result ans;
        ans.value = value + other.value;
        ans.location = location + other.location;
        return ans;
    }
   
    Result& operator+=(const Result& other)
    {
        value += other.value;
        location +=  other.location;
        return *this;
    }
    
    bool operator<(const Result& other)
    {
        return value < other.value;
    }
};

bool isOpposite(char letter, char prevLetter)
{
    return ((letter == 'W' && prevLetter == 'E') ||  
            (letter == 'E' && prevLetter == 'W') || 
            (letter == 'N' && prevLetter == 'S') ||
            (letter == 'S' && prevLetter == 'N'));
}

Result countBrackets(const Result& result, std::set<Direction>& result2)
{
    std::list<Result> results;
    std::stack<char> path;
    results.push_back(Result());
    char letter;
    while(std::cin >> letter)
    {
        if(letter == ')')
            break;
        else if(letter == '|')
        {
            path = std::stack<char>();
            results.push_back(Result());
        }
        else if(letter == '(')
            results.back() += countBrackets(result + results.back(), result2);
        else
        {
            Direction direction = DirectionUtil::isDirection(letter);
            if(path.empty() || !isOpposite(letter, path.top()))
            {                
                path.push(letter);
                results.back().value++;
                results.back().location += direction;
                if((result + results.back()).value >= PATH_LENGTH)
                    result2.insert((result + results.back()).location);
            }
            else if(!path.empty())
            {
                results.back().value--;
                results.back().location += direction;
                path.pop();
            }
        }
    }
    return *std::max_element(results.begin(), results.end());
}

int main(void)
{
    char letter;
    Result result;
    std::set<Direction> result2;
    while(std::cin >> letter)
    {
        if(letter == '^')
            continue;
        else if(letter == '$')
            break;
        else if(letter == '(')
            result += countBrackets(result, result2);
        else 
        {
            Direction direction = DirectionUtil::isDirection(letter);
            result.value++;
            result.location += direction;
            if(result.value >= PATH_LENGTH)
                result2.insert(result.location);
        }
    }
    std::cout << "First part: " << result.value << std::endl;
    std::cout << "Second part: " << result2.size() << std::endl;
    return 0;
}