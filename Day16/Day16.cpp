#include<sstream>
#include<iostream>
#include<string>
#include<algorithm>
#include<utility>
#include<tuple>
#include<map>
#include<set>
#include<list>

using namespace std;



struct Registers
{
    int reg0, reg1, reg2, reg3;
    int * arr[4];

    Registers(int a=0, int b=0, int c=0, int d= 0) : 
       reg0(a), reg1(b), reg2(c), reg3(d)
    {
        arr[0] = &reg0;
        arr[1] = &reg1;
        arr[2] = &reg2;
        arr[3] = &reg3;
    }

    Registers(const Registers& copy)
    {
        reg0 = copy.reg0;
        reg1 = copy.reg1;
        reg2 = copy.reg2;
        reg3 = copy.reg3;

        arr[0] = &reg0;
        arr[1] = &reg1;
        arr[2] = &reg2;
        arr[3] = &reg3;
    }

    Registers & operator=(Registers& copy)
    {
        if(this == &copy)
            return *this;

        reg0 = copy.reg0;
        reg1 = copy.reg1;
        reg2 = copy.reg2;
        reg3 = copy.reg3;
        return *this;
    }

    int & operator[](int index)
    {
        return *arr[index];
    }

    const int & operator[](int index) const
    {
        return *arr[index];
    }
    
    bool operator==(const Registers& other) const
    {
        return other.reg0 == reg0 &&
            other.reg1 == reg1 &&
            other.reg2 == reg2 &&
            other.reg3 == reg3;
    }
};

#define A 0
#define B 1
#define C 2
typedef tuple< int, int, int > Arguments;

enum OpCode
{
    addr,
    addi,
    mulr,
    muli,
    banr,
    bani,
    borr,
    bori,
    setr,
    seti,
    gtir,
    gtri,
    gtrr,
    eqir,
    eqri,
    eqrr
};

typedef Registers (*Function)(Arguments arg, Registers reg);

Registers Addr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] + reg[get<B>(arg)];
    return reg;
}

Registers Addi(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] + get<B>(arg);
    return reg;
}

Registers Mulr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] * reg[get<B>(arg)];
    return reg;
}

Registers Muli(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] * get<B>(arg);
    return reg;
}

Registers Banr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] & reg[get<B>(arg)];
    return reg;
}

Registers Bani(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] & get<B>(arg);
    return reg;
}

Registers Borr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] | reg[get<B>(arg)];
    return reg;
}

Registers Bori(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] | get<B>(arg);
    return reg;
}

Registers Setr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)];
    return reg;
}

Registers Seti(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = get<A>(arg);
    return reg;
}

Registers Gtir(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = get<A>(arg) > reg[get<B>(arg)] ? 1 : 0;
    return reg;
}

Registers Gtri(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] > get<B>(arg) ? 1 : 0;
    return reg;
}

Registers Gtrr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] > reg[get<B>(arg)] ? 1 : 0;
    return reg;
}

Registers Eqir(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = get<A>(arg) == reg[get<B>(arg)] ? 1 : 0;
    return reg;
}

Registers Eqri(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] == get<B>(arg) ? 1 : 0;
    return reg;
}

Registers Eqrr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] == reg[get<B>(arg)] ? 1 : 0;
    return reg;
}

void GetRegisters(string& input, Registers& registers)
{
    auto it1 = find(input.begin(), input.end(), '[');
    auto it2 = find(it1+1, input.end(), ',');
    registers[0] = stoi(std::string(it1+1,it2));
    it1 = find(it2+1, input.end(), ',');
    registers[1] = stoi(std::string(it2+2,it1));
    it2 = find(it1+1, input.end(), ',');
    registers[2] = stoi(std::string(it1+2,it2));
    it1 = find(it2+1, input.end(), ']');
    registers[3] = stoi(std::string(it2+2,it1));
}

void FindOpCodes(map< int, OpCode >& opCodeMap, map< int, set< OpCode > >& possibilities, int level)
{
    if(opCodeMap.size() == possibilities.size())
        return;
    for( auto it = possibilities[level].begin(); it != possibilities[level].end(); it++)
    {
        bool opCodeUsed = false;
        for(int lowerLevel = 0; lowerLevel < level; lowerLevel++)
        {
            if(*it == opCodeMap[lowerLevel])
            {
                opCodeUsed = true;
                break;
            }
        }
        if(opCodeUsed)
            continue;

        opCodeMap[level] = *it;
        FindOpCodes(opCodeMap, possibilities, level + 1);
        if(opCodeMap.size() == possibilities.size())
            break;
    }
    return;
}

int main(void)
{
    map<OpCode, Function> functions;
    functions[addr] = &Addr;
    functions[addi] = &Addi;
    functions[mulr] = &Mulr;
    functions[muli] = &Muli;
    functions[banr] = &Banr;
    functions[bani] = &Bani;
    functions[borr] = &Borr;
    functions[bori] = &Bori;
    functions[setr] = &Setr;
    functions[seti] = &Seti;
    functions[gtir] = &Gtir;
    functions[gtri] = &Gtri;
    functions[gtrr] = &Gtrr;
    functions[eqir] = &Eqir;
    functions[eqri] = &Eqri;
    functions[eqrr] = &Eqrr;

    string input;
    map< int, set< OpCode > > possibilities;
    map< int, OpCode > opCodeMap;
    while( getline(cin, input) )
    {
        if(input.find("Before") != string::npos)
        {
            int numberBehave = 0;

            int opCode;
            Registers before;
            Arguments args;
            Registers after;

            GetRegisters(input, before);

            getline(cin, input);
            istringstream in(input);
            in >> opCode >> get<A>(args) >> get<B>(args) >> get<C>(args);

            getline(cin, input);
            GetRegisters(input, after);

            for(auto it = functions.begin(); it != functions.end(); it++)
            {
                if(it->second(args,before) == after)
                {
                    possibilities[opCode].insert(it->first);
                }
            }
        }
        else if( input.size() > 0 )
        {
            break;
        }
    }

    FindOpCodes(opCodeMap, possibilities, 0);


    Registers reg;
    do
    {
        istringstream in(input);
        int opCode;
        Arguments args;

        in >> opCode >> get<A>(args) >> get<B>(args) >> get<C>(args);
        reg = functions[ opCodeMap[opCode] ](args, reg);
    }
    while(getline(cin,input));

    cout << reg.reg0 << endl;
    return 0;
}