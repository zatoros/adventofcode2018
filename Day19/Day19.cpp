#include<sstream>
#include<iostream>
#include<string>
#include<algorithm>
#include<utility>
#include<tuple>
#include<map>
#include<vector>

using namespace std;



struct Registers
{
    int reg0, reg1, reg2, reg3, reg4, reg5;
    int * arr[6];

    Registers(int a=0, int b=0, int c=0, int d= 0, int e= 0, int f= 0) : 
       reg0(a), reg1(b), reg2(c), reg3(d), reg4(e), reg5(f)
    {
        arr[0] = &reg0;
        arr[1] = &reg1;
        arr[2] = &reg2;
        arr[3] = &reg3;
        arr[4] = &reg4;
        arr[5] = &reg5;
    }

    Registers(const Registers& copy)
    {
        reg0 = copy.reg0;
        reg1 = copy.reg1;
        reg2 = copy.reg2;
        reg3 = copy.reg3;
        reg4 = copy.reg4;
        reg5 = copy.reg5;

        arr[0] = &reg0;
        arr[1] = &reg1;
        arr[2] = &reg2;
        arr[3] = &reg3;
        arr[4] = &reg4;
        arr[5] = &reg5;
    }

    Registers & operator=(const Registers& copy)
    {
        if(this == &copy)
            return *this;

        reg0 = copy.reg0;
        reg1 = copy.reg1;
        reg2 = copy.reg2;
        reg3 = copy.reg3;
        reg4 = copy.reg4;
        reg5 = copy.reg5;
        return *this;
    }

    int & operator[](int index)
    {
        return *arr[index];
    }

    const int & operator[](int index) const
    {
        return *arr[index];
    }
    
    bool operator==(const Registers& other) const
    {
        return other.reg0 == reg0 &&
            other.reg1 == reg1 &&
            other.reg2 == reg2 &&
            other.reg3 == reg3 &&
            other.reg4 == reg4 &&
            other.reg5 == reg5;
    }
};

#define A 0
#define B 1
#define C 2
typedef tuple< int, int, int > Arguments;

typedef Registers (*Function)(Arguments arg, Registers reg);

Registers Addr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] + reg[get<B>(arg)];
    return reg;
}

Registers Addi(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] + get<B>(arg);
    return reg;
}

Registers Mulr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] * reg[get<B>(arg)];
    return reg;
}

Registers Muli(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] * get<B>(arg);
    return reg;
}

Registers Banr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] & reg[get<B>(arg)];
    return reg;
}

Registers Bani(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] & get<B>(arg);
    return reg;
}

Registers Borr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] | reg[get<B>(arg)];
    return reg;
}

Registers Bori(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] | get<B>(arg);
    return reg;
}

Registers Setr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)];
    return reg;
}

Registers Seti(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = get<A>(arg);
    return reg;
}

Registers Gtir(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = get<A>(arg) > reg[get<B>(arg)] ? 1 : 0;
    return reg;
}

Registers Gtri(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] > get<B>(arg) ? 1 : 0;
    return reg;
}

Registers Gtrr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] > reg[get<B>(arg)] ? 1 : 0;
    return reg;
}

Registers Eqir(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = get<A>(arg) == reg[get<B>(arg)] ? 1 : 0;
    return reg;
}

Registers Eqri(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] == get<B>(arg) ? 1 : 0;
    return reg;
}

Registers Eqrr(Arguments arg, Registers reg)
{
    reg[get<C>(arg)] = reg[get<A>(arg)] == reg[get<B>(arg)] ? 1 : 0;
    return reg;
}

struct Instruction
{
    string opCode;
    Arguments args; 
};

int main(void)
{
    map<string, Function> functions;
    functions["addr"] = &Addr;
    functions["addi"] = &Addi;
    functions["mulr"] = &Mulr;
    functions["muli"] = &Muli;
    functions["banr"] = &Banr;
    functions["bani"] = &Bani;
    functions["borr"] = &Borr;
    functions["bori"] = &Bori;
    functions["setr"] = &Setr;
    functions["seti"] = &Seti;
    functions["gtir"] = &Gtir;
    functions["gtri"] = &Gtri;
    functions["gtrr"] = &Gtrr;
    functions["eqir"] = &Eqir;
    functions["eqri"] = &Eqri;
    functions["eqrr"] = &Eqrr;

    vector<Instruction> instructions;

    int instructionRegister = 0;

    string input;
    {
        getline(cin,input);
        istringstream in (input);
        string trash;
        in >> trash;
        in >> instructionRegister;
    }
    
    while(getline(cin, input))
    {
        istringstream in (input);
        Instruction inst;
        in >> inst.opCode;
        in >> get<A>(inst.args);
        in >> get<B>(inst.args);
        in >> get<C>(inst.args);
        instructions.push_back(inst);
    }

    int instructionPointer = 0;
    Registers regs;
    //Task 2
    // regs.reg0 = 1;
	// static int i = 0;
    while(instructionPointer < static_cast<int>(instructions.size()))
    {
		//Just for test
		//if (i++ > 10000)
		//	break;
        //cout << instructionPointer 
		//                                     << "   [" << regs.reg0 << "  " 
        //                                     << regs.reg1 << "  " 
        //                                     << regs.reg2 << "  " 
        //                                     << regs.reg3 << "  " 
        //                                     << regs.reg4 << "  " 
        //                                     << regs.reg5 << "]" << endl;
        regs[instructionRegister] = instructionPointer;
        regs = functions[instructions[instructionPointer].opCode](instructions[instructionPointer].args, regs);
        instructionPointer = regs[instructionRegister];
        instructionPointer++;
    }

    cout << regs.reg0 << endl;

	//In task 2 reg0 is equals of every factorial of 10551355 so reg0 = sum (
	    //1
		//5
		//499
		//4229
		//2495
		//21145
		//2110271
		//10551355)

    return 0;
}